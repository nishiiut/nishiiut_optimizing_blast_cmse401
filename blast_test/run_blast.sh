#!/bin/bash
#SBATCH -N 3
#SBATCH -c 2
#SBATCH --mem 10gb
#SBATCH --time 02:00:00

# load the necessary module
module load BLAST+/2.7.1

# unzip compressed files so the software can access them
gunzip db/sequence.fasta.gz
gunzip scd1_data/gene.fna.gz

# Convert sequence.fasta, which contains chromosome 19, into a searchable database by using the makeblast command
makeblastdb -in db/sequence.fasta -dbtype nucl -parse_seqids

# run the blastn software to search for the scd1 gene in chromosome 19
blastn -query scd1_data/gene.fna -db db/sequence.fasta -out scd1_results.txt

# remove unnecessary files after job is done to restore original state
rm db/sequence.fasta.*

# zip uncompressed files after job is done to restore original state
gzip db/sequence.fasta
gzip scd1_data/gene.fna

# show how much resources the job actually used
scontrol show job $SLURM_JOB_ID

# nishiiut_Optimizing_BLAST_CMSE401

# Abstract:

BLAST is a suite of software tools for sequence analysis that is used to search nucleotide or protein sequence databases for sequences that match a query sequence of interest. BLAST+ is a version of BLAST that can be run from the command line. The tool can be used to automate sequence analysis tasks or integrate them into larger pipelines. It can be used for a wide range of applications, including genome annotation, comparative genomics, and protein structure prediction. BLAST+ has been widely adopted in the scientific community for its speed and accuracy. This tutorial will provide instructions on how to install Blast+ and run it on a sample data set.

# Installation instructions

To install Blast+ on an HPCC (High-Performance Computing Cluster) system, you can follow these general steps:

First, check if Blast+ is already installed by running the command `module avail` to see if Blast+ is already available in the list of installed software modules. The MSU HPCC cluster has Blast pre-installed. 

If Blast+ is not already installed, you will need to download and install it manually by following the steps below:

1. Download the Blast+ source code from the NCBI website: https://blast.ncbi.nlm.nih.gov/doc/blast-help/downloadblastdata.html [1]

2. Extract the files from the downloaded archive using a command like `tar xzf ncbi-blast-2.12.0+-x64-linux.tar.gz`.

3. Move the extracted directory to a location where you want to keep the software

4. Load the necessary modules using commands like `module load gcc` and `module load zlib`.

5. Compile the software using the provided makefiles: `cd ncbi-blast-2.12.0+-src/c++; make all_r`.

6. Set up environment variables to use the Blast+ tools. The exact variables you need to set depend on your shell and the installation directory. Some example commands include:

`export PATH=/opt/software/blast+/ncbi-blast-2.12.0+/bin:$PATH`   
`export BLASTDB=/path/to/blast/databases`

# Example code

In this example, we will be search for the SCD1 gene in chromosome 9. 

1. git clone this repository
2. The fasta file for the SCD1 gene is located in the scd1_data folder [2]. The reference chromosome (on which the search will be conducted) is located in the db folder [3].
3. Run the bash script by running sbatch `run_blast.sh`
4. There will be two files generated from the run: (1) scd1_results.txt, which contains the results from the run, and (2) slurm-7922831.out, which contains the run resource usage and run speed.

run_blast.sh has two main components:
1. The makeblastdb command is used to format the file for use with BLAST+ [4]
2. Run a BLAST search using the blastn [4]


# Submission script
The submission script is called `run_blast.sh`

# References
1. https://blast.ncbi.nlm.nih.gov/doc/blast-help/downloadblastdata.html
2. https://www.ncbi.nlm.nih.gov/nuccore/NC_000085.7?report=fasta&from=44382889&to=44396148&strand=true
3. https://www.ncbi.nlm.nih.gov/nuccore/NC_000085.7
4. https://www.ncbi.nlm.nih.gov/books/NBK569839/
